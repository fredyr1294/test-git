<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$errors         = array();  	// array to hold validation errors
$data 			= array(); 		// array to pass back data

require "formvalidator.php";
$validator = new validator();

// validate the variables ======================================================
	if (empty($_POST['name'])){
		$errors['name'] = 'El Nombre es requerido.';
	}else{
		if(!$validator -> validatorname($_POST['name'])){
			$errors['name'] = 'El Nombre no es correcto';
		}
	}
	if (empty($_POST['email'])){
		$errors['email'] = 'El Correo electrónico es requerido.';
	}else{
		if(!$validator -> validatoremail($_POST['email'])){
			$errors['email'] = 'El Correo electrónico no es correcto';
		}
	}
	if (empty($_POST['phone'])){
		$errors['phone'] = 'El Teléfno es requerido.';
	}else{
		if(!$validator -> validatorphone($_POST['phone'])){
			$errors['phone'] = 'El Teléfono no es correcto, debe contener 10 números ';
		}
	}
	if (empty($_POST['message'])){
		$errors['message'] = 'El Mensaje es requerido.';
	}

	// return a response ===========================================================
	if(empty($errors)){
		$name = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$message = $_POST['message'];
		$date = date("Y-m-d");
		$type = "ISI Solutions";

		$email_to = 'pedro.tavares@dapis.mx';
		$subject = "ISI Solutions - Contacto desde página web";
		$message_body = "Datos del contacto \r\n";
		$message_body .= "Nombre: $name \r\n";
		$message_body .= "Email: $email \r\n";
		$message_body .= "Teléfono: $phone \r\n";
		$message_body .= "Mensaje: $message \r\n";

		// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";

		// Cabeceras adicionales
		$headers .= 'To:<'.$email_to."\r\n";
		$headers .= 'From: ISI Solutions <contacto@isisolutions.com.mx>'."\r\n";

		// Enviarlo
		if(mail($email_to, $subject, $message_body, $headers)){
			$ok = 'El mensaje se envio correctamente.';
		}else{
			$errors['sendmail'] = 'Ocurrio un error al enviar el mensaje, intentelo más tarde.';
		}

		$curl_post_data=['name'=>$name, 'email'=> $email, 'phone'=> $phone, 'message'=> $message, 'date'=> $date, 'type'=> $type];

		$url = 'https://isisolutions.com.mx/isi_rediseno/dbcontacto.php';
		$cliente = curl_init ();
		curl_setopt ($cliente, CURLOPT_URL, $url);
		curl_setopt ($cliente, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cliente, CURLOPT_POSTFIELDS, $curl_post_data);
		$consulta = curl_exec ($cliente);
		$error = curl_error($cliente);
		curl_close ($cliente);
		echo $error;
		$resultado = json_decode($consulta);
		$data['db'] = $resultado;
	}

	// if there are any errors in our errors array, return a success boolean of false
	if ( ! empty($errors)) {
		// if there are items in our errors array, return those errors
		$data['success'] = false;
		$data['errors']  = $errors;
	} else {
		$data['success'] = true;
		$data['message'] = $ok;
	}

// return all our data to an AJAX call
	echo json_encode($data);
?>